#include <Wire.h>
#include <Adafruit_Sensor.h>
#include "Adafruit_BMP3XX.h"
#include <LiquidCrystal_I2C.h>
#include <SimpleDHT.h>
#include <avr/sleep.h>
#include <avr/wdt.h>
#include <EEPROM.h>


#define DEBUG 1

const int wakeUpPin = 2;
const int wakeUpPinConf = 3;
const int lcdPower = 9;
const int plusPin = 5;
const int minusPin = 6;
//const int bmpPower = 8;
const byte LED = 4;
int pinDHT11 = 8;
long unsigned conftimer = millis();

const long InternalReferenceVoltage = 1062;  // Adjust this value to your board's specific internal BG voltage

int turn = 0;
bool dispon = false;
bool confdisplay = false;
float altcor;
float pressure_vals[13];

Adafruit_BMP3XX bmp;

byte upup[] = {
  B00100,
  B01110,
  B11111,
  B00000,
  B00100,
  B01110,
  B11111,
  B00000
};

byte up[] = {
  B00000,
  B00000,
  B00100,
  B01110,
  B11111,
  B00000,
  B00000,
  B00000
};

byte equal[] = {
  B00000,
  B00000,
  B00000,
  B11111,
  B11111,
  B00000,
  B00000,
  B00000
};

byte down[] = {
  B00000,
  B00000,
  B00000,
  B11111,
  B01110,
  B00100,
  B00000,
  B00000
};

byte downdown[] = {
  B00000,  
  B11111,
  B01110,
  B00100,
  B00000,
  B11111,
  B01110,
  B00100
};

byte bat1[] = {
  B00111,
  B01000,
  B10011,
  B10111,
  B10111,
  B10011,
  B01000,
  B00111
};


byte bat2[] = {
  B11111,
  B00000,
  B11111,
  B11111,
  B11111,
  B11111,
  B00000,
  B11111
};

byte bat3[] = {
  B11100,
  B00010,
  B11001,
  B11101,
  B11101,
  B11001,
  B00010,
  B11100
};

 
SimpleDHT11 dht11(pinDHT11);

LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE); 

void from_eeprom (void *ptr,unsigned char dim,unsigned char start_position)
{
 unsigned char k,mom;
 for (k=0;k<dim;k++)
 {
 mom = EEPROM.read(start_position+k);
 ((unsigned char*) ptr)[k]=mom;
 }    
 
 return;
}

void to_eeprom (void *ptr,unsigned char dim,unsigned char start_position)
{
 unsigned char k,mom;
 for (k=0;k<dim;k++)
 {
 mom = ((unsigned char*) ptr)[k];
 EEPROM.write(start_position+k,mom);
 }    
 
 return;
}

// watchdog interrupt
ISR (WDT_vect) 
{
   wdt_disable();  // disable watchdog
}  // end of WDT_vect

void wakeUp()
{
  // cancel sleep as a precaution
  sleep_disable();
  // precautionary while we do other stuff
  detachInterrupt (0);
  detachInterrupt (1);
  // Just a handler for the pin interrupt.
  dispon=true;
}

void wakeUpConf()
{
  // cancel sleep as a precaution
  sleep_disable();
  // precautionary while we do other stuff
  detachInterrupt (0);
  detachInterrupt (1);
  // Just a handler for the pin interrupt.
  confdisplay=true;
}

void update_pressure(float (val)) {
  for (int i=1; i<13; i++){
    pressure_vals[i-1] = pressure_vals[i];
  }
  pressure_vals[12] = val;
}

float cmpfunc (const void * a, const void * b) {
   return ( *(float*)a - *(float*)b );
}

float get_trend (int start){
  float past_vals[3];
  float current_vals[3];
  int n = 0;
  for (int i=start;i<start+3;i++) {
    past_vals[n] = pressure_vals[i];
    n++;
  }
  n = 0;
  for (int i=10;i<13;i++) {
    current_vals[n] = pressure_vals[i];
    n++;
  }
  qsort(past_vals,3,sizeof(float),cmpfunc);
  qsort(current_vals,3,sizeof(float),cmpfunc);
  float p = past_vals[1];
  float c = current_vals[1];
  return c-p;
}

String get_forecast(float t, float p) {
  String result;
  if (p < 1009.00) {
    if (t >= 0.0) {
      result = "cooler"; //clearing or cooler weather;
    }else if (t >= -1.50 && t < 0.0) {
      result = "rain"; //rain
    }else if (t < 1.5) {
      result = "storm"; //storm
    }
  }else if (p >= 1009.00 && p <= 1022.00){
    if (t >= -1.50 && t <= 1.50) {
      result = "stable"; //stable
    }else if (t > 1.50) {
      result = "clearng"; //clearing
    }else if (t < -1.50) {
      result = "rain"; //rain
    }
  }else if (p > 1022.00){
    if (t <-1.50) {
      result = "change"; //change
    }else if (t >= -1.50 && t < 0) {
      result = "fair"; //fair weather
    }else if (t >= 0) {
      result = "dry"; //dry weather
    }
  }
  return result;
}

// Code courtesy of "Coding Badly" and "Retrolefty" from the Arduino forum
// results are Vcc * 100
// So for example, 5V would be 500.
int getBandgap () 
  {
  // REFS0 : Selects AVcc external reference
  // MUX3 MUX2 MUX1 : Selects 1.1V (VBG)  
   ADMUX = bit (REFS0) | bit (MUX3) | bit (MUX2) | bit (MUX1);
   ADCSRA |= bit( ADSC );  // start conversion
   while (ADCSRA & bit (ADSC))
     { }  // wait for conversion to complete
   int results = (((InternalReferenceVoltage * 1024) / ADC) + 5) / 10; 
   return results;
  } // end of getBandgap

void flashBattery ()
  {
  pinMode (LED, OUTPUT);
  for (byte i = 0; i < 10; i++)
    {
    digitalWrite (LED, HIGH);
    delay (50);
    digitalWrite (LED, LOW);
    delay (50);
    }
    
  pinMode (LED, INPUT);
    
  }  // end of flash

void setup_bmp(){
  bmp.begin_I2C();
  // Set up oversampling and filter initialization
  bmp.setTemperatureOversampling(BMP3_OVERSAMPLING_8X);
  bmp.setPressureOversampling(BMP3_OVERSAMPLING_4X);
  bmp.setIIRFilterCoeff(BMP3_IIR_FILTER_COEFF_3);
  //bmp.setOutputDataRate(BMP3_ODR_50_HZ);
  bmp.performReading();
  delay(200);
  bmp.performReading();
}

void setup_lcd(){
  lcd.begin(16, 2); 
  lcd.createChar(0, downdown);
  lcd.createChar(1, down);
  lcd.createChar(2, equal);
  lcd.createChar(3, up);
  lcd.createChar(4, upup);
  lcd.createChar(5, bat1);
  lcd.createChar(6, bat2);
  lcd.createChar(7, bat3);
  lcd.clear(); 
}

void battery_sym() {
  getBandgap();
  int b = getBandgap();
  if (b > 360) {
    lcd.write((byte)5);
  }
  if (b > 375) {
    lcd.write((byte)6);
  } 
  if (b > 400) {
    lcd.write((byte)7);
  }
}

void watchdogEnable() {
      // disable ADC
    ADCSRA = 0;  

    // clear various "reset" flags
    MCUSR = 0;     
    // allow changes, disable reset
    WDTCSR = bit (WDCE) | bit (WDE);
    // set interrupt mode and an interval 
    WDTCSR = bit (WDIE) | bit (WDP3) | bit (WDP0);    // set WDIE, and 8 seconds delay
    wdt_reset();  // pat the dog
  
    set_sleep_mode (SLEEP_MODE_PWR_DOWN);  
    sleep_enable();

    noInterrupts ();           // timed sequence follows

    // will be called when pin D2 goes low  
    attachInterrupt (0, wakeUp, FALLING);
    attachInterrupt (1, wakeUpConf, FALLING);
    
    EIFR = bit (INTF0);  // clear flag for interrupt 0
    EIFR = bit (INTF1);  // clear flag for interrupt 1
    
    // turn off brown-out enable in software
    MCUCR = bit (BODS) | bit (BODSE);
    MCUCR = bit (BODS); 
    interrupts ();             // guarantees next instruction executed
    sleep_cpu ();  
  
    // cancel sleep as a precaution
    sleep_disable();    

}

void configure() {
  float newaltcor;
  newaltcor = altcor;
  conftimer = millis();
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Pressure corr."); 
  lcd.setCursor( 0, 1 );  
  lcd.print(newaltcor);
  while (true) {    
    long unsigned current_time = millis();
    if ((current_time - conftimer) > 5000) {
      if (newaltcor != altcor) {
        altcor = newaltcor;
        to_eeprom(&altcor,4,0x00);
      }
//      getBandgap();
//      lcd.clear();
//      lcd.setCursor(0, 0);
//      lcd.print("Battery level");
//      lcd.setCursor(0, 1);
//      lcd.print(getBandgap());
//      delay(3000);
      return;
    }
    if ((current_time - conftimer) > 250) {
      int readplus = digitalRead(plusPin);
      int readminus = digitalRead(minusPin);
      //int readesc = digitalRead(wakeUpPinConf);
      if (readplus == LOW) {
        newaltcor += 0.125;
        lcd.clear();
        lcd.setCursor(0, 0);
        lcd.print("Pressure corr."); 
        lcd.setCursor( 0, 1 );  
        lcd.print(newaltcor);
        conftimer = millis();
      } else if (readminus == LOW) {
        newaltcor -= 0.125;
        if (newaltcor < 0) {
          newaltcor = 0;
        }
        lcd.clear();
        lcd.setCursor(0, 0);
        lcd.print("Pressure corr."); 
        lcd.setCursor( 0, 1 );  
        lcd.print(newaltcor);
        conftimer = millis();
      }
    }
  //delay(1000);
  }
}

void load_config() {
  
}

void setup() {
  pinMode(wakeUpPin, INPUT_PULLUP); 
  pinMode(wakeUpPinConf, INPUT_PULLUP);
  pinMode(lcdPower, OUTPUT);
  pinMode(plusPin, INPUT_PULLUP);
  pinMode(minusPin, INPUT_PULLUP);
  //dht11.setPinInputMode(INPUT_PULLUP); //pullup already included in sensor
  // turn off I2C
  TWCR &= ~(bit(TWEN) | bit(TWIE) | bit(TWEA));

  // turn off I2C pull-ups
  digitalWrite (A4, LOW);
  digitalWrite (A5, LOW);

  from_eeprom (&altcor, 4, 0x00);
  for (int i = 0; i<13; i++) {
    pressure_vals[i] == bmp.pressure / 100.0;
  }
}

void loop() {
  if (dispon == true) {
    Wire.begin();
    //digitalWrite(bmpPower, HIGH);
    byte temperature = 0;
    byte humidity = 0;
    dht11.read(&temperature, &humidity, NULL);
    delay(1000);
    digitalWrite(lcdPower, HIGH);
    setup_bmp();
    setup_lcd();
    float t = bmp.temperature;
    float p = bmp.pressure / 100.0;
    float trd = get_trend(0);
    float trd2 = get_trend(6);
    //int tint = float_to_int(t,3,1);

    int err = SimpleDHTErrSuccess;
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print(t);
    lcd.print("\337C");
    if ((err = dht11.read(&temperature, &humidity, NULL)) == SimpleDHTErrSuccess) {
      lcd.setCursor( 8, 0 );
      lcd.print((int)humidity);
      lcd.print("%H ");
      }
    battery_sym();
    lcd.setCursor( 0, 1 );
    lcd.print(round(p+altcor));
    lcd.print("h ");
    if (trd <= -1.5) {
      lcd.write((byte)0);
    }else if (trd > -1.5 && trd < -0.1) {
      lcd.write((byte)1);
    }else if (trd >= -0.1 && trd <= 0.1) {
      lcd.write((byte)2);
    }else if (trd > 0.1 && trd <1.5) {
      lcd.write((byte)3);
    }else if (trd >= 1.5) {
      lcd.write((byte)4);
    }
    if (trd2 <= -1.5) {
      lcd.write((byte)0);
    }else if (trd2 > -1.5 && trd2 < -0.1) {
      lcd.write((byte)1);
    }else if (trd2 >= -0.1 && trd2 <= 0.1) {
      lcd.write((byte)2);
    }else if (trd2 > 0.1 && trd2 <1.5) {
      lcd.write((byte)3);
    }else if (trd2 >= 1.5) {
      lcd.write((byte)4);
    }
    lcd.print(" ");
    String forecast = get_forecast(trd2, p+altcor);
    lcd.print(forecast);
    delay(8000);
    digitalWrite(lcdPower, LOW);
    //digitalWrite(bmpPower, LOW);
    // turn off I2C
    TWCR &= ~(bit(TWEN) | bit(TWIE) | bit(TWEA));

    // turn off I2C pull-ups
    digitalWrite (A4, LOW);
    digitalWrite (A5, LOW);
    dispon = false;
  }else if (confdisplay == true) {
    Wire.begin();
    //digitalWrite(bmpPower, HIGH);
    digitalWrite(lcdPower, HIGH);
    delay(200);
    setup_lcd(); 
    configure();
    //delay(8000);
    digitalWrite(lcdPower, LOW);
    //digitalWrite(bmpPower, LOW);
    // turn off I2C
    TWCR &= ~(bit(TWEN) | bit(TWIE) | bit(TWEA));

    // turn off I2C pull-ups
    digitalWrite (A4, LOW);
    digitalWrite (A5, LOW);
    confdisplay = false;
  }else{
    if (turn >= 225) { //8s x 225 = 30 minutes
      Wire.begin();
      //digitalWrite(bmpPower, HIGH);
      digitalWrite(lcdPower, HIGH);
      delay(200);
      setup_bmp();
      float p = bmp.pressure / 100.0;
      update_pressure(p+altcor);
      turn = 0;
      //delay(200);
      digitalWrite(lcdPower, LOW);
      //digitalWrite(bmpPower, LOW);
      // turn off I2C
      TWCR &= ~(bit(TWEN) | bit(TWIE) | bit(TWEA));
      // turn off I2C pull-ups
      digitalWrite (A4, LOW);
      digitalWrite (A5, LOW);
    }
    turn++;
  } 
  getBandgap(); 
  if (getBandgap() < 360) { // If voltage below 3 volts blink led
    flashBattery();
  }
  watchdogEnable();
}
